﻿using CQRSPractice.Models;
using MediatR;

namespace CQRSPractice.Commands
{
    public class CreateLokasiAgunanCommand : IRequest<KodeKabLokasiAgunans>
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public bool? Is_active { get; }
        public bool? Is_delete { get; }
        public string? Created_by { get; }
        public string? Updated_by { get; }

        public CreateLokasiAgunanCommand(string code, string? name, string? description, bool? is_active, bool? is_delete, string? created_by, string? updated_by)
        {
            Code = code;
            Name = name;
            Description = description;
            Is_active = is_active;
            Is_delete = is_delete;
            Created_by = created_by;
            Updated_by = updated_by;
        }
    }
}
