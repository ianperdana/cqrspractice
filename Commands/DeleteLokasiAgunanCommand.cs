﻿using MediatR;

namespace CQRSPractice.Commands
{
    public class DeleteLokasiAgunanCommand : IRequest<int>
    {
        public int Id { get; set; }
    }
}
