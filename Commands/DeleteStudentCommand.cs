﻿using MediatR;

namespace CQRSPractice.Commands
{
    public class DeleteStudentCommand : IRequest<int>
    {
        public int Id { get; set; }
    }
}
