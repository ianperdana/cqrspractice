﻿using MediatR;

namespace CQRSPractice.Commands
{
    public class UpdateLokasiAgunanCommand : IRequest<int>
    {
        private bool? is_active;
        private bool? is_delete;
        private DateTime? updated_date;
        private string? updated_by;

        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }

        public UpdateLokasiAgunanCommand(int id,string code, string name, string description, string updatedBy)
        {
            Id = id;
            Code = code;
            Name = name;
            Description = description;
            UpdatedBy = updatedBy;
        }

        
    }
}
