﻿namespace CQRSPractice.Models
{
    public class KodeKabLokasiAgunans
    {
        public int id { get; set; }
        public string code { get; set; } = null!;
        public string? name { get; set; }
        public string? description { get; set; }
        public bool? is_active { get; set; }
        public bool? is_delete { get; set; }
        public DateTime? created_date { get; set; }
        public string? created_by { get; set; }
        public DateTime? updated_date { get; set; }
        public string? updated_by { get; set; }
    }
}
