﻿using CQRSPractice.Models;
using CQRSPractice.Queries;
using CQRSPractice.Repositories;
using MediatR;

namespace CQRSPractice.Handlers
{
    public class GetLokasiAgunanByIdHandler : IRequestHandler<GetLokasiAgunanByIdQuery, KodeKabLokasiAgunans>
    {
        private readonly IKodeKabLokasiAgunanRepository _kodeKabLokasiAgunanRepository;
        public GetLokasiAgunanByIdHandler(IKodeKabLokasiAgunanRepository kodeKabLokasiAgunanRepository)
        {
            _kodeKabLokasiAgunanRepository = kodeKabLokasiAgunanRepository;
        }
        public async Task<KodeKabLokasiAgunans> Handle(GetLokasiAgunanByIdQuery query, CancellationToken cancellationToken)
        {
            return await _kodeKabLokasiAgunanRepository.GetKodeKabLokasiAgunanByIdAsync(query.Id);
        }
    }
}
