﻿using CQRSPractice.Commands;
using CQRSPractice.Models;
using CQRSPractice.Repositories;
using MediatR;

namespace CQRSPractice.Handlers
{
    public class CreateLokasiAgunanHandler : IRequestHandler<CreateLokasiAgunanCommand, KodeKabLokasiAgunans>
    {
        private readonly IKodeKabLokasiAgunanRepository _kodeKabLokasiAgunanRepository;
        public CreateLokasiAgunanHandler(IKodeKabLokasiAgunanRepository kodeKabLokasiAgunanRepository)
        {
            _kodeKabLokasiAgunanRepository = kodeKabLokasiAgunanRepository;
        }

        public async Task<KodeKabLokasiAgunans> Handle(CreateLokasiAgunanCommand command, CancellationToken cancellationToken)
        {
            var kodeKabLokasiAgunan = new KodeKabLokasiAgunans()
            {
                code = command.Code,
                name = command.Name,
                description = command.Description,
                is_active = command.IsActive,
                is_delete = command.IsDelete,
                created_date = DateTime.Now,
                created_by = command.CreatedBy,
                updated_date = DateTime.Now,
                updated_by = command.UpdatedBy,
            };

            return await _kodeKabLokasiAgunanRepository.AddKodeKabLokasiAgunanAsync(kodeKabLokasiAgunan);
        }
    }
}
