﻿using CQRSPractice.Models;
using CQRSPractice.Queries;
using CQRSPractice.Repositories;
using MediatR;

namespace CQRSPractice.Handlers
{
    public class GetLokasiAgunanListHandler : IRequestHandler<GetLokasiAgunanListQuery, List<KodeKabLokasiAgunans>>
    {
        private readonly IKodeKabLokasiAgunanRepository _kodeKabLokasiAgunanRepository;
        public GetLokasiAgunanListHandler(IKodeKabLokasiAgunanRepository kodeKabLokasiAgunanRepository)
        {
            _kodeKabLokasiAgunanRepository = kodeKabLokasiAgunanRepository;
        }

        public async Task<List<KodeKabLokasiAgunans>> Handle(GetLokasiAgunanListQuery query, CancellationToken cancellationToken)
        {
            return await _kodeKabLokasiAgunanRepository.GetKodeKabLokasiAgunanListAsync(query.page, query.pageSize);
        }

    }
    
}
