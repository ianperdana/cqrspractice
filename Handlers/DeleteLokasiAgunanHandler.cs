﻿using CQRSPractice.Commands;
using CQRSPractice.Repositories;
using MediatR;

namespace CQRSPractice.Handlers
{
    public class DeleteLokasiAgunanHandler : IRequestHandler<DeleteLokasiAgunanCommand, int>
    {
        private readonly IKodeKabLokasiAgunanRepository _kodeKabLokasiAgunanRepository;
        public DeleteLokasiAgunanHandler(IKodeKabLokasiAgunanRepository kodeKabLokasiAgunanRepository)
        {
            _kodeKabLokasiAgunanRepository = kodeKabLokasiAgunanRepository;
        }
        public async Task<int> Handle(DeleteLokasiAgunanCommand command, CancellationToken cancellationToken)
        {
            var lokasiAgunan = await _kodeKabLokasiAgunanRepository.GetKodeKabLokasiAgunanByIdAsync(command.Id);
            if (lokasiAgunan == null)
                return default;

            return await _kodeKabLokasiAgunanRepository.DeleteKodeKabLokasiAgunanAsync(lokasiAgunan.id);
        }
    }
}
