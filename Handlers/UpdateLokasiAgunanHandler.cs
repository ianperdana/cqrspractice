﻿using CQRSPractice.Commands;
using CQRSPractice.Repositories;
using MediatR;

namespace CQRSPractice.Handlers
{
    public class UpdateLokasiAgunanHandler : IRequestHandler<UpdateLokasiAgunanCommand, int>
    {
        private readonly IKodeKabLokasiAgunanRepository _kodeKabLokasiAgunanRepository;
        public UpdateLokasiAgunanHandler(IKodeKabLokasiAgunanRepository kodeKabLokasiAgunanRepository)
        {
            _kodeKabLokasiAgunanRepository = kodeKabLokasiAgunanRepository;
        }

        public async Task<int> Handle(UpdateLokasiAgunanCommand command, CancellationToken cancellationToken)
        {
            var lokasiAgunan = await _kodeKabLokasiAgunanRepository.GetKodeKabLokasiAgunanByIdAsync(command.Id);
            if (lokasiAgunan == null)
                return default;

            lokasiAgunan.code = command.Code;
            lokasiAgunan.name = command.Name;
            lokasiAgunan.description = command.Description;
            lokasiAgunan.is_active = command.IsActive;
            lokasiAgunan.is_delete = command.IsDelete;
            lokasiAgunan.created_date = command.CreatedDate;
            lokasiAgunan.created_by = command.CreatedBy;
            lokasiAgunan.updated_date = command.UpdatedDate;

            return await _kodeKabLokasiAgunanRepository.UpdateKodeKabLokasiAgunanAsync(lokasiAgunan);
        }
    }
}
