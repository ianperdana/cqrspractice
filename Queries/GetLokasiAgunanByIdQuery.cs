﻿using CQRSPractice.Models;
using MediatR;

namespace CQRSPractice.Queries
{
    public class GetLokasiAgunanByIdQuery : IRequest<KodeKabLokasiAgunans>
    {
        public int Id { get; set; }
    }
}
