﻿using CQRSPractice.Models;
using MediatR;

namespace CQRSPractice.Queries
{
    public class GetStudentListQuery : IRequest<List<StudentDetails>>
    {
        public int page { get; set; }
        public int pageSize { get; set; }
    }
}
