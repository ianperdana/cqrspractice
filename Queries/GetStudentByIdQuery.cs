﻿using CQRSPractice.Models;
using MediatR;

namespace CQRSPractice.Queries
{
    public class GetStudentByIdQuery : IRequest<StudentDetails>
    {
        public int Id { get; set; }
    }
}
