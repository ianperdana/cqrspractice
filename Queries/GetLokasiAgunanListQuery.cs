﻿using CQRSPractice.Models;
using MediatR;

namespace CQRSPractice.Queries
{
    public class GetLokasiAgunanListQuery : IRequest<List<KodeKabLokasiAgunans>>
    {
        public int page { get; set; }
        public int pageSize { get; set; }
    }
}
