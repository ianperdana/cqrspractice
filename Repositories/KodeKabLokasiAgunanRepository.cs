﻿using CQRSPractice.Data;
using CQRSPractice.Models;
using Microsoft.EntityFrameworkCore;

namespace CQRSPractice.Repositories
{
    public class KodeKabLokasiAgunanRepository : IKodeKabLokasiAgunanRepository
    {
        private readonly DbContextClass _dbContext;

        public KodeKabLokasiAgunanRepository(DbContextClass dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<KodeKabLokasiAgunans> AddKodeKabLokasiAgunanAsync(KodeKabLokasiAgunans model)
        {
            var result = _dbContext.KodeKabLokasiAgunans.Add(model);
            await _dbContext.SaveChangesAsync();
            return result.Entity;
        }

        public async Task<int> DeleteKodeKabLokasiAgunanAsync(int Id)
        {
            var filteredData = _dbContext.KodeKabLokasiAgunans.Where(x => x.id == Id).FirstOrDefault();
            filteredData.is_delete = true;
            filteredData.is_active = false;
            _dbContext.KodeKabLokasiAgunans.Update(filteredData);
            return await _dbContext.SaveChangesAsync();
        }

        public async Task<KodeKabLokasiAgunans> GetKodeKabLokasiAgunanByIdAsync(int Id)
        {
            return await _dbContext.KodeKabLokasiAgunans.Where(x => x.id == Id).FirstOrDefaultAsync();
        }

        public async Task<List<KodeKabLokasiAgunans>> GetKodeKabLokasiAgunanListAsync(int page, int pageSize)
        {
            int skip = (page - 1) * pageSize;
            return await _dbContext.KodeKabLokasiAgunans.Skip(skip).Take(pageSize).ToListAsync();
        }

        public async Task<int> UpdateKodeKabLokasiAgunanAsync(KodeKabLokasiAgunans model)
        {
            _dbContext.KodeKabLokasiAgunans.Update(model);
            return await _dbContext.SaveChangesAsync();
        }
    }
}
