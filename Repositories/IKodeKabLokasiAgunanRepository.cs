﻿using CQRSPractice.Models;

namespace CQRSPractice.Repositories
{
    public interface IKodeKabLokasiAgunanRepository
    {
        public Task<List<KodeKabLokasiAgunans>> GetKodeKabLokasiAgunanListAsync(int page, int pageSize);
        public Task<KodeKabLokasiAgunans> GetKodeKabLokasiAgunanByIdAsync(int Id);
        public Task<KodeKabLokasiAgunans> AddKodeKabLokasiAgunanAsync(KodeKabLokasiAgunans studentDetails);
        public Task<int> UpdateKodeKabLokasiAgunanAsync(KodeKabLokasiAgunans studentDetails);
        public Task<int> DeleteKodeKabLokasiAgunanAsync(int Id);
    }
}
