﻿using CQRSPractice.Commands;
using CQRSPractice.Models;
using CQRSPractice.Queries;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CQRSPractice.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class KodeKabLokasiAgunanController : ControllerBase
    {
        private readonly IMediator mediator;

        public KodeKabLokasiAgunanController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpGet]
        public async Task<List<KodeKabLokasiAgunans>> GetLokasiAgunanListAsync(int page = 1, int pageSize = 10)
        {
            var lokasiAgunans = await mediator.Send(new GetLokasiAgunanListQuery() { page = page, pageSize = pageSize });
            return lokasiAgunans;

        }

        [HttpGet("LokasiAgunanId")]
        public async Task<KodeKabLokasiAgunans> GetStudentByIdAsync(int LokasiAgunanId)
        {
            var KodeKabLokasiAgunans = await mediator.Send(new GetLokasiAgunanByIdQuery() { Id = LokasiAgunanId });

            return KodeKabLokasiAgunans;
        }

        [HttpPost]
        public async Task<KodeKabLokasiAgunans> AddStudentAsync(KodeKabLokasiAgunans KodeKabLokasiAgunans)
        {
            var kodeKabLokasiAgunan = await mediator.Send(new CreateLokasiAgunanCommand(
                KodeKabLokasiAgunans.code,
                KodeKabLokasiAgunans.name,
                KodeKabLokasiAgunans.description,
                KodeKabLokasiAgunans.is_active,
                KodeKabLokasiAgunans.is_delete,
                KodeKabLokasiAgunans.created_by,
                KodeKabLokasiAgunans.updated_by
                ));
            return kodeKabLokasiAgunan;
        }

        [HttpPut]
        public async Task<int> UpdateStudentAsync(KodeKabLokasiAgunans KodeKabLokasiAgunans)
        {
            var isStudentDetailUpdated = await mediator.Send(new UpdateLokasiAgunanCommand(
               KodeKabLokasiAgunans.id,
               KodeKabLokasiAgunans.code,
               KodeKabLokasiAgunans.name,
               KodeKabLokasiAgunans.description,
               KodeKabLokasiAgunans.updated_by
                ));
            return isStudentDetailUpdated;
        }

        [HttpDelete]
        public async Task<int> DeleteStudentAsync(int Id)
        {
            return await mediator.Send(new DeleteLokasiAgunanCommand() { Id = Id });
        }
    }
}
